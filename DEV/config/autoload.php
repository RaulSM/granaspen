<?php 
/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 06/07/2016
 * Time: 1:05
 */
$credentials = require_once 'credentials.php';
$GLOBALS['credentials'] = $credentials;

// Configurar objeto conexion bbbdd
$bbddObject = new \stdClass();
$bbddObject->bbddUser = $credentials['db_user'];
$bbddObject->bbddPassword = $credentials['db_pwd'];
$bbddObject->bbddServerName = $credentials['db_server'];
$bbddObject->bbddName = $credentials['db_table'];
$bbddObject->bbddPort = $credentials['db_port'];
$bbddObject->bbddPrefix = $credentials['db_prefix'];
$GLOBALS['db'] = $bbddObject;

return $bbddObject;
/*

// Cargar la conexion
use core\DatabaseConnection as Data;
$connection = new Data($bbddObject);
$GLOBALS['db'] = $connection;


// Cargar manipulacion bbdd
require_once PATH_CORE_CLASS.'DatabaseModification.php';
$databaseModification = new DatabaseModification($bbddObject);
$GLOBALS['db_mod'] = $databaseModification;
*/