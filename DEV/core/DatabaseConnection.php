<?php 
/**
 * Clase para manipular la connection bbdd
 *
 * @author Raúl Sánchez Mondéjar
 * @date   2015/03/11
 * @version 1.0
 */
class DatabaseConnection {

    public $connection;
    private $query;
    public $serverName;
    public $serverPort;
    public $serverUser;
    public $serverPassword;
    public $serverDatabaseName;


    function __construct($bbddObject) {


        $this->serverName = $bbddObject->bbddServerName;
        $this->serverPort = $bbddObject->bbddPort;
        $this->serverUser = $bbddObject->bbddUser;
        $this->serverPassword = $bbddObject->bbddPassword;
        $this->serverDatabaseName = $bbddObject->bbddName;

        $this->setConnection();


    }


    public function getServerName() {

        return $this->serverName;
    }


    public function getServerPort() {

        return $this->serverPort;
    }


    public function getServerUser() {

        return $this->serverUser;
    }


    public function getServerPassword() {

        return $this->serverPassword;
    }


    public function getServerDatabaseName() {

        return $this->serverDatabaseName;
    }


    public function getDataConection() {

        $items = array(
            'serverName' => $this->serverName,
            'serverPort' => $this->serverPort,
            'serverUser' => $this->serverUser,
            'serverPassword' => $this->serverPassword,
            'serverDatabaseName' => $this->serverDatabaseName
        );

        return $items;
    }


    public function setQuery($query) {

        $this->query = $query;
    }


    public function getQuery() {

        return $this->query;
    }


    /**
     * Configurar la conexion
     */
    public function setConnection() {

        $this->connection = new mysqli($this->getServerName(), $this->getServerUser(), $this->getServerPassword(), $this->getServerDatabaseName(), $this->getServerPort());

        mysqli_set_charset($this->connection, 'utf8');

    }


    private function getResults($pretty = false) {
        
        if ($results = $this->connection->query($this->getQuery())) {

            for ($rows = array(); $row = $results->fetch_assoc();) {
                array_push($rows, $row);
            }
        }

        if ($pretty) {
            if (count($rows) === 1) {
                $rows = $rows[ 0 ];
            }
        }


        if (count($rows) === 0) {
            $rows = null;
        }

        return $rows;
    }


    private function getResultsArray() {

        if ($results = $this->connection->query($this->getQuery())) {

            for ($rows = array(); $row = $results->fetch_array();) {

                array_push($rows, $row);

            }
        }

        return $rows;
    }


    private function simpleConnection() {

        return $this->getResults($this->getQuery());

    }


    public function update() {

        $query = $this->getQuery();

        if (!$query) {
            return null;
        }

        return $this->simpleConnection();
    }


    public function read() {

        $query = $this->getQuery();

        if (!$query) {
            return null;
        }


        return $this->simpleConnection();

    }


    public function getColumnsName($table) {

        $query = "SHOW COLUMNS FROM $table";

        $this->setQuery($query);

        $resultado = $this->connection->query($this->getQuery());

        $data_array = array();

        while ($row = mysqli_fetch_assoc($resultado)) {

            $data_array[ $row[ 'Field' ] ] = $row[ 'Type' ];


        }

        return $data_array;

    }

    public function searchByID($table, $id) {

        $query = "SELECT * FROM $table WHERE id = $id";

        $this->setQuery($query);

        $results = $this->getResults();

        if (!empty($results)) {
            $results = $results[ 0 ];
        }

        return $results;
    }


    public function getResultsFromTableName($table) {

        $query = "SELECT * FROM $table";

        $this->setQuery($query);

        return $this->getResults();
    }


    public function getResultsFromTableNameArray($table) {

        $query = "SELECT * FROM $table";

        $this->setQuery($query);

        return $this->getResultsArray();
    }


    public function query($query) {

        return $this->connection->query($query);
    }


    public function getResultsFromTableNameJoin($table1, $table2) {

        $query = "SELECT * FROM $table1 " .
            "JOIN $table2 ON $table2.id_$table2 = $table1.$table2";

        $this->setQuery($query);

        return $this->getResults();

    }


    public function readPagination($index = 0, $maxElements = 20) {

        $query = $this->getQuery();

        $query .= " limit $index, $maxElements ";

        $this->setQuery($query);

        return $this->getResults();
    }


    public function countFromTable($table) {

        $query = "SELECT count(*) as 'count' FROM $table";

        $this->setQuery($query);

        $result = $this->getResults(true);

        return (int)$result[ 'count' ];
    }
}