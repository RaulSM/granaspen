<?php
require_once '../models/Users.php';

/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 06/07/2016
 * Time: 1:52
 */
class RegisterUser {


    private $errors = [
        'post-empty' => 'Ha ocurrido un error en el envio',
        'user-exist' => 'El usuario ya esta registrado',
        'insert-error' => 'Error al insertar en la base de datos',
        'email-no-validated' => 'El email indicado no ha sido validado',
        'user-no-validated' => 'No hemos podido validar su email y contraseña',
    ];
    private $messages = [
        'insert-correct' => 'El usuario ha sido insertado correctamente',
        'user-validated' => 'El usuario ha sido validado',
        'user-ok' => 'El email y la contraseñan son validos',
    ];

    public function create($request) {

        $response = [];
        $request = $this->checkRequest($request);


        if(!empty($request['errors'])) {
            return $response['errors'] = $request['errors'];
        }


        if (isset($request['password2'])) {
            unset($request['password2']);
        }

        $userModel = new Users();
        
        $userExist = $userModel->userExist($request['email']);

        if($userModel) {
            // comprobar si necesita validar por email
            if($userExist['remember_token'] != '') {
                return $response['errors'] = $this->errors['email-no-validated'];
            }
            return $response['errors'] = $this->errors['user-exist'];
        }

        // Si no existe, se da de alta
        $token = $userModel->create($request);
        if($token === false) {
            return $response['errors'] = $this->errors['insert-error'];
        }

        $urlValidation = $GLOBALS['credentials']['app_url']."register-user/validate/" . $token;

        // Enviar email
        $to     = 'nobody@example.com';
        $title    = 'Validar usuario GRANASPEN';
        $body   = "<p>El necesario que valide el usuario a través del siguiente enlace:</p>".
                    "<p><a href='". $urlValidation . "'></a></p>";
        $headers = 'From: webmaster@example.com' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $title, $body, $headers);

        return $this->messages['insert-correct'];

    }


    public function read($id) {

    }

    public function update($id, $request) {

    }


    public function delete($id) {

    }

    private function checkRequest($request) {
        // Comprobar si el post ha venido vacio
        if (empty($request)) {
            return $request['errors'] = $this->errors['post-empty'];
        }

        // Limpiar los post
        $postCleaned = [];
        foreach ($_POST as $key => $value ) {
            $postCleaned[$key] = filter_input(INPUT_POST,$key );
        }
        unset($key);
        unset($value);

        return $postCleaned;
    }

    public function validate($token) {

        $userModel = new Users();

        if ($userModel->tokenExist($token) === true) {
            return $this->messages['user-validated'];
        }

        return $this->errors['user-no-validated'];

    }
    
    public function login ($request) {
        $response = [];
        $userModel = new Users();

        if($userModel->login($request) === true) {
            return $this->messages['user-ok'];
        }

        return $this->errors['user-no-validated'];

    }
}