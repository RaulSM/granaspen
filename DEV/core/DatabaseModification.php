<?php 
require_once 'DatabaseConnection.php';
/**


 * Clase para manipular el contenido de la base de datos
 *
 * @author Raúl Sánchez Mondéjar
 * @date   2015/03/11
 * @version 1.0
 */

class DatabaseModification extends DatabaseConnection{

    function __construct($bbddObject) {
        parent::__construct($bbddObject);

    }


    public function actualizarCamposTablaPorID($tabla,$id,$datos){

        $columns = $this->connection->getColumnsName($tabla);

        if(array_key_exists('id',$columns)){
            $where = "  WHERE id = $id";
        }else{
            $where = "  WHERE id_$tabla = $id";
        } 

        $query = "UPDATE $tabla SET ";

        foreach($datos as $dato => $valor){
            $query .= "`$dato` = '".addslashes($valor)."' ,";
        }

        $query = rtrim($query, ",");        

        $query .= $where;

        // Realizar connection
        $results = $this->connection->query($query);

        return $results;
    }


    public function insertar($tabla,$datos){

        $query = "INSERT INTO $tabla ( ";

        foreach($datos as $dato => $valor ){
            $query .= "`$dato` ,";
        }

        $query = rtrim($query, ",");        

        $query .= ") VALUES ( ";

        foreach($datos as $dato => $valor){
            $query .= "'".addslashes($valor)."' ,";
        }

        $query = rtrim($query, ",");        

        $query .= ")";

        // Realizar connection
        $results = $this->connection->query($query);

        return $results;
    }


    public function eliminaRegistro($tabla,$id){

        $columns = $this->connection->getColumnsName($tabla);

        if(array_key_exists('id',$columns)){
            $where = " WHERE id = $id";
        }else{
            $where = " WHERE id_$tabla = $id";
        }

        $query = "DELETE FROM $tabla $where ";

        // Realizar connection
        $results = $this->connection->query($query);

        return $results;
    }

}