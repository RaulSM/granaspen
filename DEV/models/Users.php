<?php
include_once '../core/DatabaseModification.php';
/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 06/07/2016
 * Time: 2:17
 */
class Users extends DatabaseModification{
    
    private $table;

    public function __construct() {

        $this->table = $GLOBALS['db']->bbddPrefix.strtolower(basename(__FILE__, '.php'));
        parent::__construct($GLOBALS['db']);
    }

    public function create($data){

        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT );
        $data['remember_token'] = bin2hex(openssl_random_pseudo_bytes(16));
        if($this->insertar($this->table, $data) !== false) {
            return $data['remember_token'];
        }
        return false;
    }
    
    public function userExist($email) {
        
        $query = 'SELECT * '.
                ' FROM ' . $this->table .
                ' WHERE email = \'' . $email . '\'';
        
        $this->setQuery($query);

        $result = $this->read($query);
        if($result > 0) {

            return $result;
        }

        return false;
    }

    public function tokenExist($token) {

        $query = 'SELECT COUNT(*) as total '.
            ' FROM ' . $this->table .
            ' WHERE remember_token = \'' . $token . '\'';

        $this->setQuery($query);

        if($this->read($query) > 0) {
            $query = 'UPDATE ' . $this->table .
                ' SET remember_token = "" '.
                ' WHERE remember_token = \'' . $token . '\'';

            $this->query($query);
            return true;
        }

        return false;
    }

    public function login($data){


        $query = 'SELECT * '.
            ' FROM ' . $this->table .
            ' WHERE email = \'' . $data['email'] . '\'' ;

        $this->setQuery($query);

        $result = $this->read($query);

        if (!password_verify($data['password'], $result['password'])) {

            return false;
        }

        return true;
    }

}