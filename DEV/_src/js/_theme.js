/**
 * Created by Raul on 05/07/2016.
 */

$("#form-signin").validate({
        rules: {
            nombre: "required",
            apellido1: "required",
            apellido2: "required",
            telefono: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            password2: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        submitHandler: function (form) {

            $.ajax({
                type: form.method,
                url: form.action,
                data: $(form).serialize(),
                success: function (data) {
                    msg = JSON.parse(data);
                    $(form).append("<div id='message'></div>");
                    $('#message').html("<h2>" + msg + "</h2>");
                }
            });
            return false;
        }
    }
);

$("#form-login").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        submitHandler: function (form) {

            $.ajax({
                type: form.method,
                url: form.action,
                data: $(form).serialize(),
                success: function (data) {
                    msg = JSON.parse(data);
                    $(form).append("<div id='message'></div>");
                    $('#message').html("<h2>" + msg + "</h2>");
                }
            });
            return false;
        }
    }
);