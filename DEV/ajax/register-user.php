<?php
/**
 * Created by PhpStorm.
 * User: Raul
 * Date: 06/07/2016
 * Time: 0:37
 */
require_once '../config/autoload.php';
require_once '../core/RegisterUser.php';


$registerUser = new RegisterUser();

$action = filter_input(INPUT_GET, 'action');
$request = $_POST;
$output = '';

if(isset($_GET['token'])) {
    $token = filter_input(INPUT_GET, 'token' );
}else {
    $token = false;
}

switch ($action){
    
    case 'create': { $output = $registerUser->create($request); } break;
    case 'read': { } break;
    case 'update': { } break;
    case 'delete': { } break;
    case 'validate': { $output = $registerUser->validate($token);} break;
    case 'login': { $output = $registerUser->login($request);} break;
}

echo json_encode($output);
exit;
